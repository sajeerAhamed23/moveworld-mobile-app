package com.techfliks.laksiridriver.FunctionlessActivities;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.techfliks.laksiridriver.FunctionClasses.My;
import com.techfliks.laksiridriver.FunctionClasses.MyLog;
import com.techfliks.laksiridriver.R;
import com.techfliks.laksiridriver.TheMainActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;


public class LoginActivity extends AppCompatActivity {

    String userId;
    SharedPreferences sp;
    Activity context;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        context=LoginActivity.this;
        My.myContext =LoginActivity.this;

        sp = getSharedPreferences("MyPrefs",Context.MODE_PRIVATE);

        TextView textView = (TextView) findViewById(R.id.textViewHeading);
        textView.setText( ("Laksiri "+sp.getString("Branch","")).toUpperCase());
        My.serverAddress =My.rootServerAddress+sp.getString("Branch","error")+"/api/";

        if(isVeryfirstTime()){
            MyLog myLog=new MyLog(context);
            myLog.logFirstTime();
            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setTitle("Branch selection");
            // I'm using fragment here so I'm using getView() to provide ViewGroup
            // but you can provide here any other instance of ViewGroup from your Fragment / Activity
            View viewInflated = LayoutInflater.from(context).inflate(R.layout.branch_alert, (ViewGroup) findViewById(android.R.id.content), false);
            // Set up the input
            final Spinner input = (Spinner) viewInflated.findViewById(R.id.spinner1);
            // Specify the type of input expected; this, for example, sets the input as a password, and will mask the text
            builder.setView(viewInflated);

            String[] items = new String[]{"Qatar", "Kuwait"};
            ArrayAdapter<String> adapter = new ArrayAdapter<>(context, android.R.layout.simple_spinner_dropdown_item, items);
            input.setAdapter(adapter);

            // Set up the buttons
            builder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(final DialogInterface dialog, int which) {

                    final String branch =input.getSelectedItem().toString().toLowerCase();


                    Thread thread = new Thread(new Runnable() {

                        @Override
                        public void run() {
                            try  {
                                String output  = getUrlContents("https://laksiri.cf/configs/?who="+branch);

                                JSONObject mainObject = null;
                                try {
                                    mainObject = new JSONObject(output);
                                    String  airperkg = mainObject.getString("air per kg");
                                    SharedPreferences.Editor editor = sp.edit();
                                    editor.putString("Branch", branch);
                                    editor.putString("air per kg", airperkg );
                                    editor.commit();

                                    //to restart
                                    My.serverAddress =My.rootServerAddress+sp.getString("Branch","error")+"/api/";

                                    Intent intent = getIntent();
                                    finish();
                                    startActivity(intent);

                                } catch (JSONException e) {
                                    finish();
                                    e.printStackTrace();
                                }
                                dialog.dismiss();

                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    });

                    thread.start();


                    //m_Text = input.getText().toString();
                }
            });
            builder.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    finish();
                    dialog.cancel();
                }
            });

            builder.show();
        }

        if(!firstTime()){
            Intent d=new Intent(LoginActivity.this, TheMainActivity.class);
            startActivity(d);
        }

        (findViewById(R.id.buttonLogin)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!My.isNetworkAvailable(LoginActivity.this))
                    return;
                checkCredentials(((TextView)findViewById(R.id.editTextUserName)).getText().toString(),
                        ((TextView)findViewById(R.id.editTextPass)).getText().toString());
            }
        });
    }

    private static String getUrlContents(String theUrl)
    {
        StringBuilder content = new StringBuilder();

        // many of these calls can throw exceptions, so i've just
        // wrapped them all in one try/catch statement.
        try
        {
            // create a url object
            URL url = new URL(theUrl);

            // create a urlconnection object
            URLConnection urlConnection = url.openConnection();

            // wrap the urlconnection in a bufferedreader
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));

            String line;

            // read from the urlconnection via the bufferedreader
            while ((line = bufferedReader.readLine()) != null)
            {
                content.append(line + "\n");
            }
            bufferedReader.close();
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
        return content.toString();
    }


    String data="";
    private void checkCredentials(final String userName, final String pass) {
        MyLog myLog=new MyLog(context);
        myLog.logLogin(userName);
        final String encryptPass=md5(pass);
        String sql="SELECT id,password FROM driver WHERE username='"+userName+"' AND password='"+encryptPass+"'";
        String url=My.clean(My.serverAddress+"selector.php?a="+sql,context);
        OkHttpClient client = new OkHttpClient();
        final String basic = "Basic " + Base64.encodeToString(My.credentialsApi.getBytes(), Base64.NO_WRAP);
        Request request = new Request.Builder()
                .url(url)
                .header("Authorization", basic)
                .header("techfliks", My.MyFingerPrint())
                .build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {}
            @Override
            public void onResponse(Call call, Response response) throws IOException {
                data =response.body().string();
                fetchData(userName,encryptPass);
            }
        });


    }

    private void fetchData(String userName, String encryptPass) {
        if(data.startsWith("null")){
            context.runOnUiThread(new Runnable() {
                public void run() {
                    Toast.makeText(LoginActivity.this, "Incorrect UserName or Password", Toast.LENGTH_LONG).show();
                    ((TextView)findViewById(R.id.editTextPass)).setText("");
                }
            });

        }else {
            try {
                JSONArray ss = new JSONArray(data);
                String passTemp="";
                for (int i=0;i<ss.length();i++){
                    JSONObject d= ss.getJSONObject(i);
                    userId=d.getString("id");
                    My.driverId=d.getString("id");
                    My.driverName=userName;
                    passTemp=d.getString("password");
                }
                if(encryptPass.equals(passTemp))
                    context.runOnUiThread(new Runnable() {
                        public void run() {
                            saveInSp(userId,((TextView)findViewById(R.id.editTextUserName)).getText().toString());
                            Intent d=new Intent(LoginActivity.this,TheMainActivity.class);
                            d.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            d.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            d.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                            startActivity(d);
                            Toast.makeText(LoginActivity.this, "Login Successful", Toast.LENGTH_SHORT).show();
                        }
                    });

            } catch (JSONException e) {
                e.printStackTrace();

                context.runOnUiThread(new Runnable() {
                    public void run() {
                        Toast.makeText(LoginActivity.this, "Incorrect UserName or Password", Toast.LENGTH_LONG).show();
                        ((TextView)findViewById(R.id.editTextPass)).setText("");
                    }
                });
            }


        }
    }

    private void saveInSp(String userId,String userName) {
        SharedPreferences.Editor editor = sp.edit();
        editor.putString("isFirstTime", "false");
        editor.putString("userId", userId);
        editor.putString("userName", userName);
        editor.commit();
    }

    private boolean isVeryfirstTime() {
        String branch=sp.getString("Branch","no");
        if(branch.equals("no"))
            return true;
        else {
            return false;
        }
    }


    private boolean firstTime() {
        String isFirstTime=sp.getString("isFirstTime","true");
        if(isFirstTime.equals("true"))
            return true;
        else {
            My.driverId=sp.getString("userId","null");
            My.driverName=sp.getString("userName","null");
            return false;
        }
    }

    private Boolean exit = false;
    @Override
    public void onBackPressed() {
        if (exit) {
            Intent a = new Intent(Intent.ACTION_MAIN);
            a.addCategory(Intent.CATEGORY_HOME);
            a.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(a);
        } else {
            Toast.makeText(this, "Press Back again to Exit.",
                    Toast.LENGTH_SHORT).show();
            exit = true;
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    exit = false;
                }
            }, 3 * 1000);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(!firstTime()){
            Intent d=new Intent(LoginActivity.this, TheMainActivity.class);
            startActivity(d);
        }
    }

    private String md5(final String s) {
        final String MD5 = "MD5";
        try {
            // Create MD5 Hash
            MessageDigest digest = java.security.MessageDigest
                    .getInstance(MD5);
            digest.update(s.getBytes());
            byte messageDigest[] = digest.digest();

            // Create Hex String
            StringBuilder hexString = new StringBuilder();
            for (byte aMessageDigest : messageDigest) {
                String h = Integer.toHexString(0xFF & aMessageDigest);
                while (h.length() < 2)
                    h = "0" + h;
                hexString.append(h);
            }
            return hexString.toString();

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return "";
    }
}
