package com.techfliks.laksiridriver;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AlertDialog;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.techfliks.laksiridriver.Models.Job;
import com.techfliks.laksiridriver.FunctionClasses.My;


public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;

    Job job;


    Button finishJob;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        finishJob=(Button)findViewById(R.id.buttonFinish);

    }



    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        //view my location
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        mMap.setMyLocationEnabled(true);
        mMap.setPadding(50,50,50,75);

        LatLng marker;
        try {
            marker = new LatLng(Double.parseDouble(job.getLat()), Double.parseDouble(job.getLang()));
        }catch (Exception ignore){
            marker = new LatLng(Double.parseDouble("25.3548"), Double.parseDouble("51.1839"));
        }
        mMap.addMarker(new MarkerOptions().position(marker).title("Pickup Point"));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(marker));
        mMap.animateCamera(CameraUpdateFactory.zoomTo(16));

        finishJob.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new AlertDialog.Builder(MapsActivity.this)
                        .setTitle(R.string.app_name)
                        .setMessage("Do you want to finish this Job?")
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                My.finishingPackageNew(MapsActivity.this,job.getKey());
                            }
                        }).setNegativeButton("No",null)
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .show();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();

        job=new Job();
        job.setKey(getIntent().getStringExtra("id"));
        job.setLat(getIntent().getStringExtra("lat"));
        job.setLang(getIntent().getStringExtra("lang"));

        (findViewById(R.id.buttonBack)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_name) {
            Intent d=new Intent(MapsActivity.this, TheMainActivity.class);
            startActivity(d);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
