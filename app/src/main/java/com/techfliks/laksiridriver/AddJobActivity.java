package com.techfliks.laksiridriver;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.DatePicker;
import android.widget.TimePicker;
import android.widget.Toast;

import com.schibstedspain.leku.LocationPickerActivity;
import com.techfliks.laksiridriver.FunctionClasses.My;
import com.techfliks.laksiridriver.databinding.ActivityAddJobBinding;

import java.util.Calendar;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class AddJobActivity extends AppCompatActivity {
    ActivityAddJobBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_add_job);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_add_job);

        binding.locBut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                locationPicker();
            }
        });

        binding.dateBut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar now = Calendar.getInstance();
                new android.app.DatePickerDialog(
                        AddJobActivity.this,
                        new android.app.DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                                Log.d("Orignal", "Got clicked-" + year + " " + month + " " + dayOfMonth);
                                String temp = year + "-" + month + "-" + dayOfMonth;
                                binding.dateBut.setText(temp);
                            }
                        },
                        now.get(Calendar.YEAR),
                        now.get(Calendar.MONTH),
                        now.get(Calendar.DAY_OF_MONTH)
                ).show();
            }
        });

        binding.timeBut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar now = Calendar.getInstance();
                new android.app.TimePickerDialog(
                        AddJobActivity.this,
                        new android.app.TimePickerDialog.OnTimeSetListener() {
                            @Override
                            public void onTimeSet(TimePicker view, int hour, int minute) {
                                Log.d("Original", "Got clicked-" + hour + ":" + minute);
                                String temp = hour + ":" + minute;
                                binding.timeBut.setText(temp);
                            }
                        },
                        now.get(Calendar.HOUR_OF_DAY),
                        now.get(Calendar.MINUTE),
                        false
                ).show();
            }
        });

    }

    private void locationPicker() {
        Intent intent = new LocationPickerActivity.Builder()
                .withLocation(41.4036299, 2.1743558)
                .withGeolocApiKey(getResources().getString(R.string.google_maps_key))
                .withSearchZone("es_IN")
                .shouldReturnOkOnBackPressed()
                .withStreetHidden()
                .withCityHidden()
                .withZipCodeHidden()
                .withSatelliteViewHidden()
                .build(getApplicationContext());

        startActivityForResult(intent, 1);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1) {
            if (resultCode == RESULT_OK) {
                double latitude = data.getDoubleExtra(LocationPickerActivity.LATITUDE, 0);
                Log.d("LATITUDE****", String.valueOf(latitude));
                double longitude = data.getDoubleExtra(LocationPickerActivity.LONGITUDE, 0);
                Log.d("LONGITUDE****", String.valueOf(longitude));
                String temp = "Lat: " + String.valueOf(latitude) + " Long: " + String.valueOf(longitude);
                binding.locBut.setText(temp);
            }
            if (resultCode == RESULT_CANCELED) {
                //Write your code if there's no result
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.add_job_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_name) {
            if(checkValidity()){
                sendAllData();
                return true;
            }else {
                Toast.makeText(this, "Please fill mandatory fields", Toast.LENGTH_SHORT).show();
                return false;
            }

        }
        return super.onOptionsItemSelected(item);
    }

    private boolean checkValidity() {
        String name = binding.name.getText().toString();
        String contact = binding.contact.getText().toString();
        return !name.equals("") && !name.equals(" ") && !contact.equals("") && !contact.equals(" ");
    }

    private void sendAllData() {
        String name = binding.name.getText().toString();
        String address = binding.address.getText().toString();
        String contact = binding.contact.getText().toString();
        String descr = binding.desc.getText().toString();
        String isAir = (binding.isAir.isChecked()) ? "1" : "0";
        String location = (binding.locBut.getText().toString().equals("SELECT LOCATION") ? "" : getLoc(binding.locBut.getText().toString()));
        String date = (binding.dateBut.getText().toString().equals("SELECT DATE") ? "" : binding.dateBut.getText().toString());
        String time = (binding.timeBut.getText().toString().equals("SELECT TIME") ? "" : binding.timeBut.getText().toString());



        String sql = "INSERT INTO cargo( name, address, contact_number, location, pickup_time, pickup_date, note, fetched, isAir, auth) VALUES ('" + name + "','" + address + "','" + contact + "','" + location + "','" + time + "','" + date + "','" + descr + "','-1','" + isAir + "','" + My.driverName + "')";
        sendToServer(sql);
    }

    private String getLoc(String val) {
        try {
            String lat=val.split(" ")[1];
            String lang=val.split(" ")[3];
            return lat+":"+lang;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    private void sendToServer(String sql) {
        String url=My.serverAddress+"executor.php?a="+sql;
        AddJobActivity.OkHttpHandler okHttpHandler= new AddJobActivity.OkHttpHandler();
        okHttpHandler.execute(My.clean(url,getApplicationContext()));
    }

    private class OkHttpHandler extends AsyncTask {

        OkHttpClient client = new OkHttpClient();
        @Override
        protected Object doInBackground(Object[] params) {
            Request.Builder builder = new Request.Builder();
            final String basic = "Basic " + android.util.Base64.encodeToString(My.credentialsApi.getBytes(), android.util.Base64.NO_WRAP);

            builder.url(params[0].toString())
                    .header("techfliks", My.MyFingerPrint())
                    .header("Authorization", basic);
            Request request = builder.build();
            try {
                Response response = client.newCall(request).execute();
                Log.e("asd",response.body().string()+"");
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(AddJobActivity.this, "New Job Added", Toast.LENGTH_SHORT).show();
                    }
                });
                return response.body().string();
            }catch (Exception e){
                e.printStackTrace();
            }
            return null;
        }
    }
}