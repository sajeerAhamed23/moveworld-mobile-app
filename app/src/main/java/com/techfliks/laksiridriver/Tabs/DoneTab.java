package com.techfliks.laksiridriver.Tabs;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.RadioButton;

import com.techfliks.laksiridriver.Adapters.Tab2Adapter;
import com.techfliks.laksiridriver.MasterActivity;
import com.techfliks.laksiridriver.Models.Job;
import com.techfliks.laksiridriver.FunctionClasses.My;
import com.techfliks.laksiridriver.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;


public class DoneTab  extends Fragment {

    ArrayList<Job> jobs;
    Tab2Adapter tabAdapter;
    Activity context;
    RadioButton today;

    private void fetchData() {
        jobs.clear();
        Job job;
        try {
            JSONArray ss = new JSONArray(data);
            for (int i=0;i<ss.length();i++){
                JSONObject d= ss.getJSONObject(i);

                job=new Job();
                job.setKey(d.getString("id"));
                job.setName(d.getString("name"));
                job.setPickupedTime(d.getString("pickedup_time"));
                job.setPickupedDate(d.getString("pickedup_date"));
                job.setTot_package(d.getString("tot_package"));
                job.setAmount(d.getString("unit_amount"));
                job.setIs_air(d.getString("isAir"));
                jobs.add(job);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        context.runOnUiThread(new Runnable() {
            public void run() {
                tabAdapter.notifyDataSetChanged();
            }
        });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.tab2, container, false);
        context=getActivity();
        jobs=new ArrayList<>();
        ListView listView=(ListView)rootView.findViewById(R.id.listView);
        tabAdapter =new Tab2Adapter(context,jobs);
        listView.setAdapter(tabAdapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent d=new Intent(context, MasterActivity.class);
                d.putExtra("id",jobs.get(i).getKey());
                d.putExtra("fromWhere","Done");
                startActivity(d);
            }
        });

        (rootView.findViewById(R.id.fab)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(IsNetworkConnected(context)){
                    dealWithServer();
                    //fetchData();
                }
            }
        });

        today=(RadioButton)rootView.findViewById(R.id.today);
        return rootView;
    }
    private boolean IsNetworkConnected(Context c) {
        ConnectivityManager cm = (ConnectivityManager)  c.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo ni = cm.getActiveNetworkInfo();
        return ni != null;
    }
    String data;
    private void dealWithServer() {
        String driveId=My.driverId;
        String timeSelected="AND pickup_date>='"+getAddTime(-7)+"'";
        if(today.isChecked()){
            timeSelected="AND pickup_date>='"+getAddTime(-1)+"'";
        }
        String sql="SELECT *,id as idk,(SELECT sum(quantity) FROM package where cargo_id=idk)as tot_package,(SELECT sum(weigth) FROM package WHERE cargo_id=idk) as tot_weight,(SELECT sum(length*breadth*heigth) FROM package WHERE cargo_id=idk) as tot_volume FROM cargo WHERE fetched=0 AND picker="+driveId+" "+timeSelected;
//        My dbHelper=new My();
//        data=dbHelper.sqlSelector(sql);
        sqlSelector(sql);
    }

    private String getAddTime(int i) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        Calendar c = Calendar.getInstance();
        c.setTime(new Date());
        c.add(Calendar.DATE, i);
        String output = sdf.format(c.getTime());
        return output;
    }


    private void sqlSelector(String sql) {
        String url=My.serverAddress+"selector.php?a="+sql;
        fresh(My.clean(url,context));
    }

    private void fresh(String url){
        OkHttpClient client = new OkHttpClient();
        final String basic = "Basic " + android.util.Base64.encodeToString(My.credentialsApi.getBytes(), android.util.Base64.NO_WRAP);
        Request request = new Request.Builder()
                .url(url)
                .header("techfliks", My.MyFingerPrint())
                .header("Authorization", basic)
                .build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {}
            @Override
            public void onResponse(Call call, Response response) throws IOException {
                data =response.body().string();
                fetchData();
            }
        });

    }
}
