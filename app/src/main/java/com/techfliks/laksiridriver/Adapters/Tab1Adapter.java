package com.techfliks.laksiridriver.Adapters;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.techfliks.laksiridriver.Models.Job;
import com.techfliks.laksiridriver.R;

import java.util.ArrayList;


public class Tab1Adapter extends ArrayAdapter<Job> {

    private final Activity context;
    private final ArrayList<Job> jobs;

    public Tab1Adapter(Activity context, ArrayList<Job> jobs) {
        super(context, R.layout.row_tab1, jobs);
        this.context = context;
        this.jobs = jobs;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();
        View rowView = inflater.inflate(R.layout.row_tab1, null, true);

        TextView rowName=(TextView)rowView.findViewById(R.id.rowName);
        TextView rowAddress=(TextView)rowView.findViewById(R.id.rowAddress);
        TextView rowConNumber=(TextView)rowView.findViewById(R.id.rowConNumber);
        TextView rowDateTime=(TextView)rowView.findViewById(R.id.rowDateTime);

        String seaAir = " / Sea";
        if (jobs.get(position).getIs_air().equals("1"))
            seaAir = " / Air";

        rowName.setText("HBL No."+jobs.get(position).getKey()+"\n"+jobs.get(position).getName()+seaAir);
        rowAddress.setText(jobs.get(position).getAddress());
        rowConNumber.setText(jobs.get(position).getContactNum());
        rowDateTime.setText(jobs.get(position).getPickupDate()+" "+jobs.get(position).getPickupTime()+" | "+jobs.get(position).getNote());

        return rowView;
    }
}