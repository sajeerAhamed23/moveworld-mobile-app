package com.techfliks.laksiridriver.Adapters;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.techfliks.laksiridriver.Models.Job;
import com.techfliks.laksiridriver.R;

import java.util.ArrayList;


public class Tab2Adapter extends ArrayAdapter<Job> {

    private final Activity context;
    private final ArrayList<Job> jobs;

    public Tab2Adapter(Activity context, ArrayList<Job> jobs) {
        super(context, R.layout.row_tab2, jobs);
        this.context = context;
        this.jobs = jobs;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();
        View rowView = inflater.inflate(R.layout.row_tab2, null, true);

        TextView rowName=(TextView)rowView.findViewById(R.id.rowName1);
        TextView rowAmount=(TextView)rowView.findViewById(R.id.rowAmount1);
        TextView rowDT=(TextView)rowView.findViewById(R.id.rowDT1);
        TextView rowNumPack=(TextView)rowView.findViewById(R.id.rowNumPack1);

        String seaAir = " / Sea";
        if (jobs.get(position).getIs_air().equals("1"))
            seaAir = " / Air";


        rowName.setText("HBL No."+jobs.get(position).getKey()+"\n"+jobs.get(position).getName()+seaAir);
        rowAmount.setText(jobs.get(position).getAmount());
        rowDT.setText(jobs.get(position).getPickupedDate()+" "+jobs.get(position).getPickupedTime());
        rowNumPack.setText(jobs.get(position).getTot_package());



        return rowView;
    }
}