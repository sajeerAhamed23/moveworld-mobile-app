package com.techfliks.laksiridriver;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.techfliks.laksiridriver.FunctionlessActivities.MainActivity;
import com.techfliks.laksiridriver.Models.Job;
import com.techfliks.laksiridriver.FunctionClasses.My;
import com.techfliks.laksiridriver.Models.Packs;
import com.techfliks.laksiridriver.PdfCreator.MyPrint;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;


public class MasterActivity extends AppCompatActivity {

    Job job;
    LinearLayout l1,l2,l3,l4,l1_1,l2_1,l3_1,l4_1;
    TextView notes,pickedDate,pickedTime,amount,name,address,contactNum,latlang,totWeight,totVolume,numPack;
    ListView packList;
    Button backButton,doneButton,navButton,printButton;
    Activity context;

    @Override
    protected void onResume() {
        super.onResume();
        job=new Job();
        String fromWhere=getIntent().getStringExtra("fromWhere");
        String temp=getIntent().getStringExtra("id");
        job.setKey(temp);
        setTitle("HBL Number "+temp);
        Log.e("id2",getIntent().getStringExtra("id"));
        visiblity(fromWhere);
        populate();
    }

    String data="";
    private void populate() {
        String sql="SELECT *,id as idk,(SELECT sum(quantity) FROM package where cargo_id=idk)as tot_package,(SELECT sum(weigth) FROM package WHERE cargo_id=idk) as tot_weight,(SELECT sum(length*breadth*heigth*quantity) FROM package WHERE cargo_id=idk) as tot_volume FROM cargo WHERE id="+job.getKey();
        sqlSelector(sql);
    }

    private void sqlSelector(String sql) {
        String url=My.serverAddress+"selector.php?a="+sql;
        fresh(My.clean(url,context));
    }

    private void fresh(String url){
        OkHttpClient client = new OkHttpClient();

        final String basic = "Basic " + Base64.encodeToString(My.credentialsApi.getBytes(), Base64.NO_WRAP);
        Request request = new Request.Builder()
                .url(url)
                .header("Authorization", basic)
                .header("techfliks", My.MyFingerPrint())
                .build();

        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {}
            @Override
            public void onResponse(Call call, Response response) throws IOException {
                data =response.body().string();
                fetchData();
            }
        });
    }

    private void fetchData() {
        try {
            JSONArray ss = new JSONArray(data);
            for (int i=0;i<ss.length();i++){
                JSONObject d= ss.getJSONObject(i);

                job.setActive(d.getString("active"));
                job.setQid(d.getString("qid"));
                job.setName(d.getString("name"));
                job.setAddress(d.getString("address"));
                job.setContactNum(d.getString("contact_number"));
                try {
                    job.setLang(d.getString("location").split(":")[0]);
                    job.setLat(d.getString("location").split(":")[1]);
                }catch (Exception ignore){
                    job.setLang("");
                    job.setLat("");
                }


                job.setPickerNote(d.getString("picker_note"));
                job.setPickupDate(d.getString("pickup_date"));
                job.setPickupTime(d.getString("pickup_time"));
                job.setPickupedDate(d.getString("pickedup_date"));
                job.setPickupedTime(d.getString("pickedup_time"));
                job.setAmount(d.getString("unit_amount"));


                job.setNote(d.getString("note"));
                job.setOfficer_note(d.getString("officer_note"));
                job.setAmount_spec(d.getString("amount_spec"));
                job.setOrdered_date(d.getString("ordered_date"));
                job.setOrdered_time(d.getString("ordered_time"));
                job.setIs_air(d.getString("isAir"));
                job.setIs_colombo(d.getString("is_colombo"));
                job.setIs_door(d.getString("is_door"));
                job.setSl_address(d.getString("sl_address"));
                job.setSl_name(d.getString("sl_name"));
                job.setSl_contact(d.getString("sl_contact"));
                job.setSl_pp_no(d.getString("sl_pp_no"));
                job.setPp_no(d.getString("pp_no"));
                job.setPaid(d.getString("paid"));



                job.setTot_weight(d.getString("tot_weight"));
                job.setTot_package(d.getString("tot_package"));
                job.setTot_volume(d.getString("tot_volume"));


            }

        } catch (JSONException e) {
            e.printStackTrace();
        }


        String sql="SELECT *,ifNull((length*breadth*heigth),0) as cmb FROM package WHERE cargo_id="+job.getKey();
        sqlSelector1(sql);

    }


    private void sqlSelector1(String sql) {
        String url=My.serverAddress+"selector.php?a="+sql;
        fresh1(My.clean(url,context));
    }

    private void fresh1(String url){
        OkHttpClient client = new OkHttpClient();
        final String basic = "Basic " + Base64.encodeToString(My.credentialsApi.getBytes(), Base64.NO_WRAP);
        Request request = new Request.Builder()
                .url(url)
                .header("Authorization", basic)
                .header("techfliks", My.MyFingerPrint())
                .build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {}
            @Override
            public void onResponse(Call call, Response response) throws IOException {
                data =response.body().string();
                fetchData1();
            }
        });

    }

    private void fetchData1() {
        final ArrayList<String> concatedArray=new ArrayList<>();
        ArrayList<Packs> packs=new ArrayList<>();
        try {
            JSONArray ss = new JSONArray(data);
            for (int i=0;i<ss.length();i++){
                JSONObject d= ss.getJSONObject(i);
                Packs pack=new Packs();
                pack.setDescr(d.getString("type"));
                pack.setSize(d.getString("length")+"X"+d.getString("breadth")+"X"+d.getString("heigth"));
                pack.setCmb(d.getString("cmb"));
                pack.setRemarks(d.getString("remarks"));
                pack.setWeight(d.getString("weigth"));
                pack.setQty(d.getString("quantity"));
                concatedArray.add(pack.getSize()+" | "+pack.getWeight()+" | "+pack.getDescr());
                packs.add(pack);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        job.setPacks(packs);

        context.runOnUiThread(new Runnable() {
            public void run() {
                updateUI(concatedArray);
            }
        });

    }

    private void updateUI(ArrayList<String> concatedArray) {
        //update UI to job
        notes.setText(job.getNote());
        pickedDate.setText(job.getPickupDate());
        pickedTime.setText(job.getPickupTime());
        amount.setText(job.getAmount());

        //update UI to job
        name.setText(job.getName());
        address.setText(job.getAddress());
        contactNum.setText(job.getContactNum());
        String latLangLoc="Location Not Available";
        if(!job.getLang().equals(""))
            latLangLoc=job.getLat()+","+job.getLang();
        latlang.setText(latLangLoc);

        ArrayAdapter<String> adapter = new ArrayAdapter<>(this,
                android.R.layout.simple_list_item_1, android.R.id.text1,concatedArray);
        packList.setAdapter(adapter);

        //updating UI
        totVolume.setText(job.getTot_volume()+" Meter cubed");
        totWeight.setText(job.getTot_weight());
        numPack.setText(job.getTot_package());
    }








    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_master);
        setTitle("Summary");
        UIconnections();

        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackClicked();
            }
        });
        printButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onPrintClicked();
            }
        });
        doneButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onDoneClicked();
            }
        });
        navButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onNavClicked();
            }
        });
    }
    private void UIconnections() {
        context=MasterActivity.this;
        l1=(LinearLayout)findViewById(R.id.lin1);
        l2=(LinearLayout)findViewById(R.id.lin2);
        l3=(LinearLayout)findViewById(R.id.lin3);
        l4=(LinearLayout)findViewById(R.id.lin4);
        l1_1=(LinearLayout)findViewById(R.id.lin1_1);
        l2_1=(LinearLayout)findViewById(R.id.lin2_1);
        l3_1=(LinearLayout)findViewById(R.id.lin3_1);
        l4_1=(LinearLayout)findViewById(R.id.lin4_1);
        backButton=(Button)findViewById(R.id.backBut);
        doneButton=(Button)findViewById(R.id.doneBut);
        navButton=(Button)findViewById(R.id.navBut);
        printButton=(Button)findViewById(R.id.printBut);
        packList=(ListView)findViewById(R.id.packageList);
        notes=(TextView)findViewById(R.id.notes);
        pickedDate=(TextView)findViewById(R.id.pickedDate);
        pickedTime=(TextView)findViewById(R.id.pickedTime);
        amount=(TextView)findViewById(R.id.amontRec);
        name=(TextView)findViewById(R.id.name);
        address=(TextView)findViewById(R.id.address);
        contactNum=(TextView)findViewById(R.id.contactNum);
        latlang=(TextView)findViewById(R.id.latlang);
        totWeight=(TextView)findViewById(R.id.totWeight);
        totVolume=(TextView)findViewById(R.id.totVol);
        numPack=(TextView)findViewById(R.id.numPack);
    }
    public void onBackClicked() {
        startActivity(new Intent(context, MainActivity.class));
    }
    public void onL1clicked(View view){
        LinearLayout lin=l1_1;
        if(lin.getVisibility()==View.VISIBLE)
            lin.setVisibility(View.GONE);
        else
            lin.setVisibility(View.VISIBLE);
    }
    public void onL2clicked(View view){
        LinearLayout lin=l2_1;
        if(lin.getVisibility()==View.VISIBLE)
            lin.setVisibility(View.GONE);
        else
            lin.setVisibility(View.VISIBLE);
    }
    public void onL3clicked(View view){
        LinearLayout lin=l3_1;
        if(lin.getVisibility()==View.VISIBLE)
            lin.setVisibility(View.GONE);
        else
            lin.setVisibility(View.VISIBLE);
    }
    public void onL4clicked(View view){
        LinearLayout lin=l4_1;
        if(lin.getVisibility()==View.VISIBLE)
            lin.setVisibility(View.GONE);
        else
            lin.setVisibility(View.VISIBLE);
    }

    public void onDoneClicked() {
        My.finishingPackageNew(context,job.getKey());
    }
    private void visiblity(String fromWhere) {
        if(fromWhere.equals("Done")){
            l1.setVisibility(View.VISIBLE);
            l2.setVisibility(View.VISIBLE);
            l3.setVisibility(View.VISIBLE);
            l4.setVisibility(View.VISIBLE);
            backButton.setVisibility(View.VISIBLE);
            doneButton.setVisibility(View.GONE);
            navButton.setVisibility(View.GONE);
            printButton.setVisibility(View.VISIBLE);
        }
        else if (fromWhere.equals("Pending")){
            l1.setVisibility(View.GONE);
            l2.setVisibility(View.VISIBLE);
            l3.setVisibility(View.VISIBLE);
            l4.setVisibility(View.VISIBLE);
            backButton.setVisibility(View.VISIBLE);
            doneButton.setVisibility(View.VISIBLE);
            navButton.setVisibility(View.VISIBLE);
            printButton.setVisibility(View.GONE);
        }
    }

    public void onPrintClicked() {
        if(job.getKey()==null)
            return;
        Date date = new Date() ;
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(date);
        String filename = timeStamp;
        MyPrint fop = new MyPrint();
        try{
            if (fop.write(filename,context,job)) {
                Toast.makeText(context, filename + ".pdf created", Toast.LENGTH_SHORT).show();
                Log.e("Done","done");
            } else {
                Toast.makeText(context, "I/O error",Toast.LENGTH_SHORT).show();
            }
        }catch (Exception e){
            e.printStackTrace();
            Toast.makeText(context, "Error in creating PDF", Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_name) {
            Intent d=new Intent(MasterActivity.this, TheMainActivity.class);
            startActivity(d);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void onNavClicked() {
        Intent d=new Intent(context, MapsActivity.class);
        d.putExtra("id",job.getKey());
        d.putExtra("lat",job.getLat());
        d.putExtra("lang",job.getLang());
        startActivity(d);
    }

}
