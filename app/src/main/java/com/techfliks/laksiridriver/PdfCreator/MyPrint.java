package com.techfliks.laksiridriver.PdfCreator;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.support.v7.app.AlertDialog;
import android.util.Log;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfWriter;
import com.journeyapps.barcodescanner.BarcodeEncoder;
import com.techfliks.laksiridriver.Models.Job;
import com.techfliks.laksiridriver.FunctionClasses.My;
import com.techfliks.laksiridriver.Models.Packs;
import com.techfliks.laksiridriver.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;


public class MyPrint {

    //hbl form
    private void drawingAll(Job job) {

        drawString(job.getPickupedDate(), 48, 70);//"Date"
        drawString(job.getPickupedTime(), 48, 85);//"Time"
        //sea/air
        String seaAir = "Sea / ";
        if (job.getIs_air().equals("1"))
            seaAir = "Air / ";
        drawString(seaAir + job.getIs_door(), 80, 98);//"cargo type"
        drawString(job.getIs_colombo(), 113, 112);//"final dest"
        drawString(job.getKey(), 48, 127);//"HBL NO"

        if(job.getName().length()>36){
            drawString(job.getName().substring(0,33), 70, 225);//"Name"
            drawString("-"+job.getName().substring(33), 70, 237);//"Name"
        }else {
            drawString(job.getName(), 70, 225);//"Name"
        }

        if(job.getAddress().length()>72){
            drawString(job.getAddress().substring(0,33), 70, 256-4);//"Address"
            drawString("-"+job.getAddress().substring(33,66), 70, 267-4);//"Address"
            drawString("-"+job.getAddress().substring(66), 70, 278-4);//"Address"
        }else if(job.getAddress().length()>36){
            drawString(job.getAddress().substring(0,33), 70, 256-4);//"Address"
            drawString("-"+job.getAddress().substring(33), 70, 267-4);//"Address"
        }else {
            drawString(job.getAddress(), 70, 256-4);//"Address"
        }
        drawString(job.getQid(), 70, 286);//"Tell No"
        drawString(job.getContactNum(), 70, 301);//"Tell No"
        drawString(job.getPp_no(), 70, 316);//"NIC NO"




        if(job.getSl_name().length()>36){
            drawString(job.getSl_name().substring(0,33), 370, 225);//"Name"
            drawString("- "+job.getSl_name().substring(33), 370, 237);//"Name"
        }else {
            drawString(job.getSl_name(), 370, 225);//"Name"
        }
        if(job.getSl_address().length()>72){
            drawString(job.getSl_address().substring(0,33), 370, 256-4);//"Address"
            drawString("- "+job.getSl_address().substring(33,66), 370, 267-4);//"Address"
            drawString("- "+job.getSl_address().substring(66), 370, 278-4);//"Address"
        }else if(job.getSl_address().length()>39){
            drawString(job.getSl_address().substring(0,33), 370, 256-4);//"Address"
            drawString("- "+job.getSl_address().substring(33), 370, 267-4);//"Address"
        }else {
            drawString(job.getSl_address(), 370, 256-4);//"Address"
        }
        drawString(job.getSl_contact(), 370, 301);//"Contact number"
        drawString(job.getSl_pp_no(), 370, 316);//"PP NO"


//        drawString(job.getQid(),515, 298);//"QID NO"

        drawString(job.getTot_package().replace("No.Packages ", ""), 97, 595);//"NO of Package"
        drawString(My.floatToZerozero(job.getTot_volume().replace("Volume ", "")), 97, 610);//"total Volume"
        drawString(job.getTot_weight(), 97, 625);//"total weight"

        /*float inc = 20.5f;
        float temp=348;
        drawString("",561, temp);//Charge1
        temp+=inc;
        drawString("",561, temp);//Charge2
        temp+=inc;
        drawString("",561, temp);//Charge3
        temp+=inc;
        drawString("",561, temp);//Charge4
        temp+=inc;
        drawString("",561, temp);//Charge5
        temp+=inc;
        drawString("",561, temp);//Charge6
        temp+=inc;
        drawString("",561, temp);//Charge7
        temp+=inc;
        drawString("",561, temp);//Charge8
        temp+=inc;
        drawString("",561, temp);//Charge9
        temp+=inc;*/


        drawString(job.getAmount(), 369, 595);//"Tot amount"
        drawString(job.getPaid(), 369, 610);//"Tot paid"

        float zero = 18;
        float one = 56;
        float two = 200;
        float three = 308;
        float four = 352;
        float five = 407;
        float six = 469;

        float yValue = 358;
        float qty = 0, cmb = 0;
        for (int i = 0; i < 17; i++) {
            if (job.getPacks().size() > i) {
                Packs pack = job.getPacks().get(i);
                drawString((i + 1) + "", zero, yValue);//"no"
                drawString(pack.getDescr(), one, yValue);//"desc"
                drawString(pack.getSize(), two, yValue);//"size"
                drawString(pack.getQty(), three, yValue);//"qty"
                drawString(pack.getWeight(), four, yValue);//"weight"
                drawString(My.floatToZerozero(pack.getCmb()), five, yValue);//"cmb"
                drawString(pack.getRemarks(), six, yValue);//"remarks"

                qty += My.f(pack.getQty());
                cmb += My.f(pack.getCmb());
            }
            yValue += 14.5;
        }
        //drawString(String.format("%.0f", qty),two,yValue);//"qty"
        //drawString(String.format("%.0f", cmb),three,yValue);//"cmb"


        drawString((My.NumberToWords(Integer.parseInt(job.getPaid().replace(" QAR", "")))).toUpperCase(), 76, 645);//"In words"


        /*drawString("",484, 641);//"Signature 1"
        drawString("",484, 674);//"Signature 2"*/


        /*drawString(My.driverName,316, 702);//"Sales person"
        drawString("-",85, 702);//"CR NO"
        drawString("",310, 723);//"Auth sign top"
        drawString("",491, 744);//"Auth sign"
        drawString(String.format("%.2f", My.f(job.getAmount())-My.f(job.getPaid())),204, 729);//"Balance"*/


        drawString(job.getNote(), 17, 767);//"Special Note"

    }

    //hbl form
    private void drawingAll_headless(Job job) {

        drawString(job.getPickupedDate() + "  " + job.getPickupedTime(), 48, 152);//"Date time" +82
        drawString(job.getKey(), 292, 154 );//"HBL NO"

        //sea/air
        String seaAir = "Sea / ";
        if (job.getIs_air().equals("1"))
            seaAir = "Air / ";
        drawString(seaAir + job.getIs_door(), 80, 98+70 );//"cargo type"
        drawString(job.getIs_colombo(), 113, 112+70 );//"final dest"

        if (job.getName().length() > 36) {
            drawString(job.getName().substring(0, 33), 70, 225 );//"Name"
            drawString("-" + job.getName().substring(33), 70, 237 );//"Name"
        } else {
            drawString(job.getName(), 70, 225 );//"Name"
        }

        if (job.getAddress().length() > 72) {
            drawString(job.getAddress().substring(0, 33), 70, 256 - 4 );//"Address"
            drawString("-" + job.getAddress().substring(33, 66), 70, 267 - 4 );//"Address"
            drawString("-" + job.getAddress().substring(66), 70, 278 - 4 );//"Address"
        } else if (job.getAddress().length() > 36) {
            drawString(job.getAddress().substring(0, 33), 70, 256 - 4 );//"Address"
            drawString("-" + job.getAddress().substring(33), 70, 267 - 4 );//"Address"
        } else {
            drawString(job.getAddress(), 70, 256 - 4 );//"Address"
        }
        drawString(job.getQid(), 70, 286 );//"Tell No"
        drawString(job.getContactNum(), 70, 301 );//"Tell No"
        drawString(job.getPp_no(), 70, 316 );//"NIC NO"


        if (job.getSl_name().length() > 36) {
            drawString(job.getSl_name().substring(0, 33), 370, 225 );//"Name"
            drawString("- " + job.getSl_name().substring(33), 370, 237 );//"Name"
        } else {
            drawString(job.getSl_name(), 370, 225 );//"Name"
        }
        if (job.getSl_address().length() > 72) {
            drawString(job.getSl_address().substring(0, 33), 370, 256 - 4 );//"Address"
            drawString("- " + job.getSl_address().substring(33, 66), 370, 267 - 4 );//"Address"
            drawString("- " + job.getSl_address().substring(66), 370, 278 - 4 );//"Address"
        } else if (job.getSl_address().length() > 39) {
            drawString(job.getSl_address().substring(0, 33), 370, 256 - 4 );//"Address"
            drawString("- " + job.getSl_address().substring(33), 370, 267 - 4 );//"Address"
        } else {
            drawString(job.getSl_address(), 370, 256 - 4 );//"Address"
        }
        drawString(job.getSl_contact(), 370, 301 );//"Contact number"
        drawString(job.getSl_pp_no(), 370, 316 );//"PP NO"


//        drawString(job.getQid(),515, 298);//"QID NO"

        drawString(job.getTot_package().replace("No.Packages ", ""), 97, 595 );//"NO of Package"
        drawString(My.floatToZerozero(job.getTot_volume().replace("Volume ", "")), 97, 610 );//"total Volume"
        drawString(job.getTot_weight(), 97, 625 );//"total weight"

        /*float inc = 20.5f;
        float temp=348;
        drawString("",561, temp);//Charge1
        temp+=inc;
        drawString("",561, temp);//Charge2
        temp+=inc;
        drawString("",561, temp);//Charge3
        temp+=inc;
        drawString("",561, temp);//Charge4
        temp+=inc;
        drawString("",561, temp);//Charge5
        temp+=inc;
        drawString("",561, temp);//Charge6
        temp+=inc;
        drawString("",561, temp);//Charge7
        temp+=inc;
        drawString("",561, temp);//Charge8
        temp+=inc;
        drawString("",561, temp);//Charge9
        temp+=inc;*/


        drawString(job.getAmount(), 369, 595 );//"Tot amount"
        drawString(job.getPaid(), 369, 610 );//"Tot paid"

        float zero = 18;
        float one = 56;
        float two = 200;
        float three = 308;
        float four = 352;
        float five = 407;
        float six = 469;

        float yValue = 358;
        float qty = 0, cmb = 0;
        for (int i = 0; i < 17; i++) {
            if (job.getPacks().size() > i) {
                Packs pack = job.getPacks().get(i);
                drawString((i + 1) + "", zero, yValue );//"no"
                drawString(pack.getDescr(), one, yValue );//"desc"
                drawString(pack.getSize(), two, yValue );//"size"
                drawString(pack.getQty(), three, yValue );//"qty"
                drawString(pack.getWeight(), four, yValue );//"weight"
                drawString(My.floatToZerozero(pack.getCmb()), five, yValue );//"cmb"
                drawString(pack.getRemarks(), six, yValue );//"remarks"

                qty += My.f(pack.getQty());
                cmb += My.f(pack.getCmb());
            }
            yValue += 14.5;
        }
        //drawString(String.format("%.0f", qty),two,yValue);//"qty"
        //drawString(String.format("%.0f", cmb),three,yValue);//"cmb"


        drawString((My.NumberToWords(Integer.parseInt(job.getPaid().replace(" QAR", "")))).toUpperCase(), 76, 645 );//"In words"


        /*drawString("",484, 641);//"Signature 1"
        drawString("",484, 674);//"Signature 2"*/


        /*drawString(My.driverName,316, 702);//"Sales person"
        drawString("-",85, 702);//"CR NO"
        drawString("",310, 723);//"Auth sign top"
        drawString("",491, 744);//"Auth sign"
        drawString(String.format("%.2f", My.f(job.getAmount())-My.f(job.getPaid())),204, 729);//"Balance"*/


        drawString(job.getNote(), 17, 767 );//"Special Note"

    }

    public Boolean write(String fname, Activity activity, Job job) {
        try {
            String fpath = "/sdcard/" + job.getKey()+"_hbl_"+ fname + ".pdf";
            File file = new File(fpath);

            if (!file.exists()) {
                file.createNewFile();
            }

            Document document = new Document(PageSize.LETTER);
            PdfWriter writer =PdfWriter.getInstance(document,
                    new FileOutputStream(file.getAbsoluteFile()));
            document.open();

            //////////////////////////////////////////////////////////////////////////////////////////
            SharedPreferences sp;
            sp = activity.getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);

            PdfContentByte canvas = writer.getDirectContentUnder();
            Drawable d = activity.getResources().getDrawable((sp.getString("Branch","").equals("kuwait")) ? R.drawable.headless_form : R.drawable.headless_form);
            BitmapDrawable bitDw = ((BitmapDrawable) d);

            //background image
            Bitmap bmp = bitDw.getBitmap();
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            bmp.compress(Bitmap.CompressFormat.PNG, 100, stream);
            Image image = Image.getInstance(stream.toByteArray());
            image.scaleAbsolute(612,792);
            image.setAbsolutePosition(0, 0);
            canvas.addImage(image);

            //barcode
            Bitmap bmp1 = barcode(job.getKey());
            ByteArrayOutputStream stream1 = new ByteArrayOutputStream();
            bmp1.compress(Bitmap.CompressFormat.PNG, 100, stream1);
            Image image1 = Image.getInstance(stream1.toByteArray());
//            image.scaleAbsolute(612,792);
            image1.setAbsolutePosition(400, 782-165); //+400
            canvas.addImage(image1);


            doc=writer.getDirectContent();
            Font arial = new Font(Font.FontFamily.TIMES_ROMAN, 10);
            bf_arial = arial.getCalculatedBaseFont(false);

            drawingAll_headless(job);
            ///////////////////////////
            document.close();

            viewPdf(activity,file);
            return true;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        } catch (DocumentException e) {
            e.printStackTrace();
            return false;
        }
    }





    //cash receipt
    private void drawingAllCashReceipt(Job job) {

        //adding 2777 to hbl number to get cash recipt number
        String cashReceiptNum= job.getKey();
        try{
            cashReceiptNum= (Integer.parseInt(job.getKey()) + 2777)+"";
        }catch (Exception e){}

        drawStringCashReceipt(cashReceiptNum, 379, 130);//"CR"
        drawStringCashReceipt(job.getPickupedTime(), 379, 143);//"Time"
        drawStringCashReceipt(job.getPickupedDate(), 379, 156);//"Date"
        drawStringCashReceipt((My.driverName).toUpperCase(), 379, 168);//"driver name"

        if(job.getName().length()>35){
            drawStringCashReceipt(job.getName().substring(0,33), 42, 145);//"Name"
            drawStringCashReceipt("-"+job.getName().substring(33), 42, 159);//"Name"
        }else {
            drawStringCashReceipt(job.getName(), 42, 145);//"Name"
        }


        //sea/air
        String seaAir = "Sea / ";
        if (job.getIs_air().equals("1"))
            seaAir = "Air / ";

        //lankan chargers paid or not
        String isLankan = "Not Paid";
        if (job.getLankanCharges().equals("1"))
            isLankan = "Paid";

        //discount
        String discount="";
        String compData= job.getAmount_spec();
        try {
            JSONArray ssIn = new JSONArray(compData);
            for (int j = 0; j < ssIn.length(); j++) {
                JSONObject d1= ssIn.getJSONObject(j);
                discount=d1.getString("DISCOUNT");
            }
        }catch (JSONException e) {
            e.printStackTrace();
        }

        drawStringCashReceipt("1", 43, 258);//"cargo type"
        drawStringCashReceipt(seaAir + job.getIs_door(), 332, 258);//"cargo type"
        drawStringCashReceipt(job.getKey(), 182, 258);//"HBL NO"
        drawStringCashReceipt(job.getPaid().replace(" QAR", ""), 500, 258);//"paid amt"

        drawStringCashReceipt((My.NumberToWords(Integer.parseInt(job.getPaid().replace(" QAR", "")))).toUpperCase(),131, 582);//"In words"

        drawStringCashReceipt("Cash", 240, 675);//"Cash/cheque"
        drawStringCashReceipt(job.getAmount(), 240, 716);//"being"


        drawStringCashReceipt(job.getAmount(), 483, 542 );//"total"
        drawStringCashReceipt(discount, 483, 554 );//"les discount"
        drawStringCashReceipt(job.getAmount(), 483, 564 );//"net"



        drawStringCashReceipt("Destination Charges "+isLankan, 53, 623 );//"being"


    }

    //cash receipt
    private void drawingAllCashReceipt_headless(Job job) {

        //adding 2777 to hbl number to get cash recipt number
        String cashReceiptNum = job.getKey();
        try {
            cashReceiptNum = (Integer.parseInt(job.getKey()) + 2777) + "";
        } catch (Exception e) {
        }

        drawStringCashReceipt(cashReceiptNum, 379, 197);//"CR" +67
        drawStringCashReceipt(job.getPickupedTime(), 379, 143 + 67);//"Time"
        drawStringCashReceipt(job.getPickupedDate(), 379, 156 + 67);//"Date"
        drawStringCashReceipt((My.driverName).toUpperCase(), 379, 168 + 67);//"driver name"

        if (job.getName().length() > 35) {
            drawStringCashReceipt(job.getName().substring(0, 33), 42, 145 + 67);//"Name"
            drawStringCashReceipt("-" + job.getName().substring(33), 42, 159 + 67);//"Name"
        } else {
            drawStringCashReceipt(job.getName(), 42, 145 + 67);//"Name"
        }


        //sea/air
        String seaAir = "Sea / ";
        if (job.getIs_air().equals("1"))
            seaAir = "Air / ";

        //lankan chargers paid or not
        String isLankan = "Not Paid";
        if (job.getLankanCharges().equals("1"))
            isLankan = "Paid";

        //discount
        String discount = "";
        String compData = job.getAmount_spec();
        try {
            JSONArray ssIn = new JSONArray(compData);
            for (int j = 0; j < ssIn.length(); j++) {
                JSONObject d1 = ssIn.getJSONObject(j);
                discount = d1.getString("DISCOUNT");
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        drawStringCashReceipt("1", 43, 258 + 67);//"cargo type"
        drawStringCashReceipt(seaAir + job.getIs_door(), 332, 258 + 67);//"cargo type"
        drawStringCashReceipt(job.getKey(), 182, 258 + 67);//"HBL NO"
        drawStringCashReceipt(job.getPaid().replace(" QAR", ""), 500, 258 + 67);//"paid amt"

        drawStringCashReceipt((My.NumberToWords(Integer.parseInt(job.getPaid().replace(" QAR", "")))).toUpperCase(), 131, 582 + 67);//"In words"

        drawStringCashReceipt("Cash", 240, 675 + 67);//"Cash/cheque"
        drawStringCashReceipt(job.getAmount(), 240, 716 + 67);//"being"


        drawStringCashReceipt(job.getAmount(), 483, 542 + 67);//"total"
        drawStringCashReceipt(discount, 483, 554 + 67);//"les discount"
        drawStringCashReceipt(job.getAmount(), 483, 564 + 67);//"net"

        drawStringCashReceipt("Destination Charges " + isLankan, 53, 623 + 67);//"being"
    }

    public Boolean writeCashReceipt(String fname, Activity activity, Job job) {
        try {
            String fpath = "/sdcard/" +job.getKey()+"_cash_"+ fname + ".pdf";
            File file = new File(fpath);

            if (!file.exists()) {
                file.createNewFile();
            }


            Document document = new Document(PageSize.A4);
            PdfWriter writer =PdfWriter.getInstance(document,
                    new FileOutputStream(file.getAbsoluteFile()));
            document.open();

            //////////////////////////////////////////////////////////////////////////////////////////
            SharedPreferences sp;
            sp = activity.getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);

            PdfContentByte canvas = writer.getDirectContentUnder();

            Drawable d1 = activity.getResources().getDrawable(R.drawable.headless_cashreceipt);
            Drawable d2 = activity.getResources().getDrawable(R.drawable.headless_cashreceipt);
            BitmapDrawable bitDw = ((BitmapDrawable) ((sp.getString("Branch","").equals("kuwait"))?d1:d2));

            //background image
            Bitmap bmp = bitDw.getBitmap();
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            bmp.compress(Bitmap.CompressFormat.PNG, 100, stream);
            Image image = Image.getInstance(stream.toByteArray());
            image.scaleAbsolute(595,841);
            image.setAbsolutePosition(0, 0);
            canvas.addImage(image);

            //barcode
//            Bitmap bmp1 = barcode(job.getKey());
//            ByteArrayOutputStream stream1 = new ByteArrayOutputStream();
//            bmp1.compress(Bitmap.CompressFormat.PNG, 100, stream1);
//            Image image1 = Image.getInstance(stream1.toByteArray());
////            image.scaleAbsolute(612,792);
//            image1.setAbsolutePosition(0, 782-165);
//            canvas.addImage(image1);


            doc=writer.getDirectContent();
            Font arial = new Font(Font.FontFamily.TIMES_ROMAN, 10);
            bf_arial = arial.getCalculatedBaseFont(false);

            drawingAllCashReceipt_headless(job);
            ///////////////////////////
            document.close();

            viewPdf(activity,file);
            return true;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        } catch (DocumentException e) {
            e.printStackTrace();
            return false;
        }
    }




//
//    private void drawingAllOld(Job job) {
//        drawString(job.getKey(),90,57);//"HBL NO"
//        drawString(job.getPickupedDate(),476, 57);//"Date Date Date"
//
//
//        drawString(job.getName(),75, 95);//"Name"
//        drawString(job.getAddress(),75, 142);//"Address"
//        drawString(job.getContactNum(),75, 170);//"Tell No"
//        drawString(job.getSl_name(),75, 195);//"Name"
//        drawString(job.getSl_contact(),75, 241);//"Contact number"
//        drawString(job.getSl_address(),75, 284);//"Address"
//        drawString(job.getSl_pp_no(),75, 300);//"PP NO"
//
//
//        drawString(job.getPp_no(),307, 298);//"NIC NO"
//        drawString("-",515, 298);//"QID NO"
//
//
//        drawString(job.getTot_package(),424, 330);//"NO of Package"
//        drawString(job.getTot_volume(),525, 330);//"total Volume"
//
//        float inc = 20.5f;
//        float temp=348;
//        drawString("",561, temp);//Charge1
//        temp+=inc;
//        drawString("",561, temp);//Charge2
//        temp+=inc;
//        drawString("",561, temp);//Charge3
//        temp+=inc;
//        drawString("",561, temp);//Charge4
//        temp+=inc;
//        drawString("",561, temp);//Charge5
//        temp+=inc;
//        drawString("",561, temp);//Charge6
//        temp+=inc;
//        drawString("",561, temp);//Charge7
//        temp+=inc;
//        drawString("",561, temp);//Charge8
//        temp+=inc;
//        drawString("",561, temp);//Charge9
//        temp+=inc;
//        drawString(job.getAmount(),561, temp);//Charge10
//
//        float zero=20;
//        float one=155;
//        float two=264;
//        float three=300;
//        float four=354;
//
//        float yValue=330;
//        float qty=0,cmb=0;
//        for (int i = 0; i < 17; i++)
//        {
//            if(job.getPacks().size()>i) {
//                Packs pack = job.getPacks().get(i);
//                drawString(pack.getDescr(), zero, yValue);//"desc"
//                drawString(pack.getSize(), one, yValue);//"size"
//                drawString(pack.getQty(), two, yValue);//"qty"
//                drawString(pack.getCmb(), three, yValue);//"cmb"
//                drawString(pack.getWeight(), four, yValue);//"remarks"
//
//                qty+= My.f(pack.getQty());
//                cmb+=My.f(pack.getCmb());
//            }
//            yValue+=14.5;
//        }
//        drawString(String.format("%.0f", qty),two,yValue);//"qty"
//        drawString(String.format("%.0f", cmb),three,yValue);//"cmb"
//
//
//        drawString(My.NumberToWords(4895),79, 597);//"In words"
//
//
//        drawString("",484, 641);//"Signature 1"
//        drawString("",484, 674);//"Signature 2"
//
//
//        drawString(My.driverName,316, 702);//"Sales person"
//        drawString("-",85, 702);//"CR NO"
//        drawString("",310, 723);//"Auth sign top"
//        drawString("",491, 744);//"Auth sign"
//        drawString(job.getAmount(),65, 729);//"Tot paid"
//        drawString(String.format("%.2f", My.f(job.getAmount())-My.f(job.getPaid())),204, 729);//"Balance"
//        drawString(job.getNote(),203, 768);//"Special Note"
//    }
    BaseFont bf_arial;
    PdfContentByte doc;
    private void drawString(String string, float x, float y) {
        try {
            doc.beginText();
            doc.setFontAndSize(bf_arial, 10);
            doc.setTextMatrix(x, 782 - y);
            doc.showText(string);
            doc.endText();
        }catch (Exception e){
            Log.e("asdf",e.getMessage());
        }
    }
    private void drawStringCashReceipt(String string, float x, float y) {
        try {
            doc.beginText();
            doc.setFontAndSize(bf_arial, 11);
            doc.setTextMatrix(x, 832 - y);
            doc.showText(string);
            doc.endText();
        }catch (Exception e){
            Log.e("asdf",e.getMessage());
        }
    }
    private Bitmap barcode(String id){
        MultiFormatWriter multiFormatWriter = new MultiFormatWriter();
        try {
            BitMatrix bitMatrix = multiFormatWriter.encode(id, BarcodeFormat.CODE_128,200,25);
            BarcodeEncoder barcodeEncoder = new BarcodeEncoder();
            Bitmap bitmap = barcodeEncoder.createBitmap(bitMatrix);
            //imageView.setImageBitmap(bitmap);
            return bitmap;
        } catch (WriterException e) {
            e.printStackTrace();
        }
        return null;
    }
    private void viewPdf(final Activity f_activity, File f_myFile){
        final Activity activity = f_activity;
        final File myFile = f_myFile;
        new AlertDialog.Builder(activity)
                .setTitle(R.string.app_name)
                .setMessage("Open?")
                .setPositiveButton("Printer App", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        Intent launchIntent = activity.getPackageManager().getLaunchIntentForPackage("pe.diegoveloper.printerserverapp");
                        if (launchIntent != null) {
                            activity.startActivity(launchIntent);//null pointer check in case package name was not found
                        }
                    }
                })
                .setNegativeButton("PDF Reader", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        Intent intent = new Intent(Intent.ACTION_VIEW);
                        intent.setDataAndType(Uri.fromFile(myFile), "application/pdf");
                        intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                        activity.startActivity(intent);
                    }
                })
                .setNeutralButton("Back",null)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();

    }
}