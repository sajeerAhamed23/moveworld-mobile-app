package com.techfliks.laksiridriver;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Handler;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.techfliks.laksiridriver.FunctionlessActivities.LoginActivity;
import com.techfliks.laksiridriver.FunctionlessActivities.MainActivity;
import com.techfliks.laksiridriver.Models.Job;
import com.techfliks.laksiridriver.FunctionClasses.My;
import com.techfliks.laksiridriver.PdfCreator.MyPrint;

import org.apache.commons.net.util.Base64;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class TheMainActivity extends AppCompatActivity{

    private WebView mWebview;
    private Button pending,addjob,newForm,logout,exitBut,print,uploadImage;
    My dbHelper;
    Activity context;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_the_main);
        context=TheMainActivity.this;
        My.myContext =TheMainActivity.this;
        pending=(Button)findViewById(R.id.pending);
        addjob=(Button)findViewById(R.id.newjob);
        newForm=(Button)findViewById(R.id.newBut);
        logout=(Button)findViewById(R.id.logout);
        exitBut=(Button)findViewById(R.id.exitBut);
        print=(Button)findViewById(R.id.printReciept);
        uploadImage=(Button)findViewById(R.id.uploadImage);


        TextView textView = (TextView) findViewById(R.id.textViewMenuHeading);
        SharedPreferences sp;
        sp = getSharedPreferences("MyPrefs",Context.MODE_PRIVATE);
        textView.setText( ("Laksiri "+sp.getString("Branch","")).toUpperCase());

        mWebview  = new WebView(this);
        dbHelper=new My();
        getLocation();

        pending.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                toApp();
            }
        });
        addjob.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                toAddJob();
            }
        });
        newForm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fillForm();
            }
        });
        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new AlertDialog.Builder(context)
                        .setTitle(R.string.app_name)
                        .setMessage("Do you want to Logout?")
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                logout();
                            }
                        }).setNegativeButton("No",null)
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .show();
            }
        });
        exitBut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                exit();
            }
        });
        print.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder alert = new AlertDialog.Builder(context);
                final EditText edittext = new EditText(context);
                edittext.setInputType(InputType.TYPE_CLASS_NUMBER);
                alert.setMessage("HBL Number?")
                        .setTitle("Print HBL")
                        .setView(edittext)
                        .setPositiveButton("Print", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                String hblNum = edittext.getText().toString();
                                //checking error
                                if(hblNum.equals(""))
                                    return;
                                try{
                                    Integer.parseInt(hblNum);
                                }catch (Exception e){
                                    return;
                                }

                                Job job=new Job();
                                job.setKey(hblNum);
                                fetchData(job);
                            }
                        })
                        .setNegativeButton("Back",null)
                        .show();
            }
        });
        uploadImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder alert = new AlertDialog.Builder(context);
                final EditText edittext = new EditText(context);
                edittext.setInputType(InputType.TYPE_CLASS_NUMBER);
                alert.setMessage("HBL Number?")
                        .setTitle("Upload Image")
                        .setView(edittext)
                        .setPositiveButton("Next", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                String hblNum = edittext.getText().toString();
                                //checking error
                                if(hblNum.equals(""))
                                    return;
                                try{
                                    Integer.parseInt(hblNum);
                                }catch (Exception e){
                                    return;
                                }
                                Intent d=new Intent(context, UploadImage.class);
                                d.putExtra("HBL_Num",hblNum);
                                startActivity(d);

                            }
                        })
                        .setNegativeButton("Close",null)
                        .show();
            }
        });
    }

    private void printReceipt(Job job) {
        Date date = new Date() ;
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(date);
        final String filename = timeStamp;
        MyPrint fop = new MyPrint();
        try{
            if(job.getKey()==null){
                runOnUiThread(new Runnable() {
                    public void run() {
                        Toast.makeText(context, "HBL number not available",Toast.LENGTH_LONG).show();
                    }
                });
            }
            else if (fop.writeCashReceipt(filename,context,job)) {
                runOnUiThread(new Runnable() {
                    public void run() {
                        Toast.makeText(context, filename + ".pdf created", Toast.LENGTH_SHORT).show();
                    }
                });
                Log.e("Done","done");
            } else {
                runOnUiThread(new Runnable() {
                    public void run() {
                        Toast.makeText(context, "I/O error",Toast.LENGTH_SHORT).show();
                    }
                });
            }
        }catch (Exception e){
            e.printStackTrace();
            runOnUiThread(new Runnable() {
                public void run() {
                    Toast.makeText(context, "Error in creating PDF", Toast.LENGTH_SHORT).show();
                }
            });
        }

    }

    private void fetchData(final Job job) {
        String sql="SELECT * FROM cargo WHERE id="+job.getKey();
        job.setKey(null); // clearing the object to fill freshly
        String url=My.clean(My.serverAddress+"selector.php?a="+sql,context);
        OkHttpClient client = new OkHttpClient();
        final String basic = "Basic " + android.util.Base64.encodeToString(My.credentialsApi.getBytes(), android.util.Base64.NO_WRAP);
        Request request = new Request.Builder()
                .url(url)
                .header("Authorization", basic)
                .header("techfliks", My.MyFingerPrint())
                .build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {}
            @Override
            public void onResponse(Call call, Response response) throws IOException {
                String data =response.body().string();
                populateJob(data,job);
            }
        });
    }

    private void populateJob(String data, Job job) {
        try {
            JSONArray ss = new JSONArray(data);
            for (int i=0;i<ss.length();i++){
                JSONObject d= ss.getJSONObject(i);
                job.setKey(d.getString("id"));
                job.setName(d.getString("name"));
                job.setPickupDate(d.getString("pickup_date"));
                job.setPickupTime(d.getString("pickup_time"));
                job.setPickupedDate(d.getString("pickedup_date"));
                job.setPickupedTime(d.getString("pickedup_time"));
                job.setIs_air(d.getString("isAir"));
                job.setIs_door(d.getString("is_door"));
                job.setPaid(d.getString("paid"));
                job.setAmount(d.getString("unit_amount"));
                job.setAmount_spec(d.getString("amount_spec"));
                job.setLankanCharges(d.getString("lankan_charges"));


            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        printReceipt(job);
    }

    private void toApp() {
        if(!My.isNetworkAvailable(context))
            return;
        Intent d=new Intent(context, MainActivity.class);
        startActivity(d);
    }

    private void toAddJob() {
        if(!My.isNetworkAvailable(context))
            return;
        Intent d=new Intent(context, AddJobActivity.class);
        startActivity(d);
    }

    private void fillForm() {
        if(!My.isNetworkAvailable(context))
            return;

        mWebview  = new WebView(this);

        mWebview.getSettings().setJavaScriptEnabled(true); // enable javascript
        mWebview.getSettings().setDomStorageEnabled(true);

        final Activity activity = this;

        mWebview.setWebViewClient(new WebViewClient() {
            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                Toast.makeText(activity, description, Toast.LENGTH_SHORT).show();
            }
        });

        String authEncoded = new String(Base64.encodeBase64(My.credentialsApp.getBytes()));
        String authHeader = "Basic " +authEncoded;
        Map<String, String> headers = new HashMap<String, String>();
        headers.put("Authorization", authHeader);
        headers.put("techfliks", My.MyFingerPrint());

        SharedPreferences sp;
        sp = getSharedPreferences("MyPrefs",Context.MODE_PRIVATE);
        mWebview.loadUrl("https://laksiri.cf/"+sp.getString("Branch","")+"/app/index.php?did="+My.driverId+"&dname="+My.driverName+"&airperkg="+ sp.getString("air per kg",""),headers);
        setContentView(mWebview );
    }

    private Boolean exit = false;
    private void exit() {
        if (exit) {
            Intent a = new Intent(Intent.ACTION_MAIN);
            a.addCategory(Intent.CATEGORY_HOME);
            a.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(a);
        } else {
            Toast.makeText(this, "Press Back again to Exit.",
                    Toast.LENGTH_SHORT).show();
            exit = true;
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    exit = false;
                }
            }, 3 * 1000);
        }
    }

    private void logout() {
        SharedPreferences sp;
        sp = getSharedPreferences("MyPrefs",Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sp.edit();
        editor.putString("isFirstTime", "true");
        editor.putString("userId", "");
        editor.putString("userName", "");
        editor.commit();

        Intent d=new Intent(context, LoginActivity.class);
        startActivity(d);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_name) {
            finish();
            startActivity(getIntent());
//            Intent d=new Intent(context, TheMainActivity.class);
//            startActivity(d);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    private LocationManager locationManager;
    private LocationListener locationListener;

    private void getLocation(){
        locationManager = (LocationManager) this.getSystemService(LOCATION_SERVICE);

        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            Log.e("Main:permission","no");
            return;
        }else{
            Log.e("Main:permmission","yes");
        }

        locationListener =new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {
                Log.e("changed","chhh");
                updateLocation(location);
            }

            @Override
            public void onStatusChanged(String s, int i, Bundle bundle) {
                Log.e("Main:locationListner","status changed");
            }

            @Override
            public void onProviderEnabled(String s) {
                Log.e("long","enabled");
            }

            @Override
            public void onProviderDisabled(String s) {
                Log.e("Main:locationListner","provider disabled");
                turnOnLocation();
            }
        };

        if (getProviderName() == null || getProviderName().equals("passive")){
            turnOnLocation();
        }else{

            locationManager.requestLocationUpdates(getProviderName(), 10000, 10, locationListener);
            Location location = locationManager.getLastKnownLocation(getProviderName());
            if (location!=null){
                updateLocation(location);
            }

        }



    }
    String getProviderName() {
        LocationManager locationManager = (LocationManager) this
                .getSystemService(Context.LOCATION_SERVICE);

        Criteria criteria = new Criteria();
        criteria.setPowerRequirement(Criteria.POWER_LOW); // Chose your desired power consumption level.
        criteria.setAccuracy(Criteria.ACCURACY_FINE); // Choose your accuracy requirement.
        criteria.setSpeedRequired(false); // Chose if speed for first location fix is required.
        criteria.setAltitudeRequired(false); // Choose if you use altitude.
        criteria.setBearingRequired(false); // Choose if you use bearing.
        criteria.setCostAllowed(false); // Choose if this provider can waste money :-)

        // Provide your criteria and flag enabledOnly that tells
        // LocationManager only to return active providers.
        String provider = locationManager.getBestProvider(criteria, true);
        try{
            Log.e("Main:Location Provider",provider);
        }catch(Exception e){}

        return provider;
    }
    private void turnOnLocation(){
        Toast.makeText(getApplicationContext(),
                "Please turn on location service",Toast.LENGTH_LONG).show();
        Intent myIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
        startActivity(myIntent);
    }

    private void updateLocation(Location location){
        //http request
        String str = location.getLatitude()+":"+location.getLongitude();
//        Toast.makeText(getBaseContext(), "Sending Location", Toast.LENGTH_SHORT).show();
        //dbHelper.sendLocation(str);

        String url=My.serverAddress+"executor.php?a=UPDATE driver SET driver_location='"+str+"' WHERE id="+My.driverId;
//        My.OkHttpHandler okHttpHandler= new My.OkHttpHandler();
//        okHttpHandler.execute(My.clean(url));

        OkHttpHandler okHttpHandler= new OkHttpHandler();
        okHttpHandler.execute(My.clean(url,context));
    }

    private class OkHttpHandler extends AsyncTask {

        OkHttpClient client = new OkHttpClient();
        @Override
        protected Object doInBackground(Object[] params) {
            Request.Builder builder = new Request.Builder();
            final String basic = "Basic " + android.util.Base64.encodeToString(My.credentialsApi.getBytes(), android.util.Base64.NO_WRAP);

            builder.url(params[0].toString())
                    .header("techfliks", My.MyFingerPrint())
                    .header("Authorization", basic);
            Request request = builder.build();
            try {
                Response response = client.newCall(request).execute();
                Log.e("asd",response.body().string()+"");
                return response.body().string();
            }catch (Exception e){
                e.printStackTrace();
            }
            return null;
        }
    }

}
