package com.techfliks.laksiridriver;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;


import com.adeel.library.easyFTP;
import com.techfliks.laksiridriver.FunctionClasses.MyLog;

import java.io.File;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

public class UploadImage extends AppCompatActivity {
    private static int RESULT_LOAD_IMAGE = 1;
    private static int CAMERA_REQUEST =2;
    //private Uri imageToUploadUri;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_upload_image);


//        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
//        StrictMode.setVmPolicy(builder.build());
//        builder.detectFileUriExposure();

        ImageView x=(ImageView)findViewById(R.id.cam);
        x.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent chooserIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                File f = new File(Environment.getExternalStorageDirectory(), "POST_IMAGE.jpg");
                chooserIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(f));
                //imageToUploadUri = Uri.fromFile(f);
                startActivityForResult(chooserIntent, CAMERA_REQUEST);
            }
        });


        ImageView x1=(ImageView)findViewById(R.id.gal);
        x1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(i, RESULT_LOAD_IMAGE);
            }
        });
    }

    String imagePath;
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RESULT_LOAD_IMAGE && resultCode == RESULT_OK && null != data) {

            imagePath = getImagePathFromGallery(getApplicationContext(), data);

            Toast.makeText(this, "Starting to Upload", Toast.LENGTH_SHORT).show();

            new uploadTask().execute(imagePath,id);


        }else if(requestCode == CAMERA_REQUEST && resultCode == RESULT_OK ) {
            File file = new File(Environment.getExternalStorageDirectory().getPath(), "POST_IMAGE.jpg");
            Uri selectedImage = Uri.fromFile(file);
            imagePath=selectedImage.getPath();

            Toast.makeText(this, "Starting to Upload", Toast.LENGTH_SHORT).show();


            new uploadTask().execute(imagePath,id);

        }

    }

    private String getImagePathFromGallery(Context context, Intent data){
        Uri selectedImage = data.getData();
        String[] filePathColumn = { MediaStore.Images.Media.DATA };
        Cursor cursor = context.getContentResolver().query(selectedImage,filePathColumn, null, null, null);
        cursor.moveToFirst();
        int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
        String picturePath = cursor.getString(columnIndex);
        cursor.close();
        return picturePath;
    }

    String id=null;
    @Override
    protected void onResume() {
        super.onResume();
        id=getIntent().getStringExtra("HBL_Num");
        if(id!=null){
            (findViewById(R.id.status)).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent d=new Intent(UploadImage.this,TheMainActivity.class);
                    startActivity(d);
                }
            });
        }
    }

    class uploadTask extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... params) {
            MyLog myLog=new MyLog(getApplicationContext());
            myLog.logFtp(params[0],params[1]);
            try {
                Date date = new Date() ;
                String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(date);

                Uri image=Uri.parse("file://" +params[0]);
                InputStream is =   getContentResolver().openInputStream(image);

                easyFTP ftp = new easyFTP();

                SharedPreferences sp;
                sp = getSharedPreferences("MyPrefs",Context.MODE_PRIVATE);
                ftp.connect("ftp.laksiri.cf",sp.getString("Branch","")+"xyz@laksiri.cf","1qazXSW@");
                boolean status=false;

                ftp.makeDir("/alldocsoflaksiri/hbl/" + params[1]);
                status=ftp.setWorkingDirectory("/alldocsoflaksiri/hbl/"+params[1]); // if User say provided any Destination then Set it , otherwise
                // Upload will be stored on Default /root level on server
//                InputStream is=getResources().openRawResource(+R.drawable.camera);
                ftp.uploadFile(is,params[1]+"_HBL_"+ timeStamp +".png");

                Log.e("reasd","success");

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(UploadImage.this, "Done Uploading", Toast.LENGTH_LONG).show();
                    }
                });
                return new String("Upload Successful");
            }catch (Exception e){
                String t="Failure : " + e.getLocalizedMessage();
                Log.e("reasd",t);
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(UploadImage.this, "Upload Failed", Toast.LENGTH_LONG).show();
                    }
                });
                return t;
            }
        }
    }
}
