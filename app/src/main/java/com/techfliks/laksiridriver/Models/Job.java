package com.techfliks.laksiridriver.Models;

import java.util.ArrayList;

public class Job {
    private String active;
    private String key;
    private String qid;
    private String name,address,lat,lang,contactNum,pickupTime,pickupDate;
    private ArrayList<Packs> packs;
    private String pickupedTime,pickupedDate,pickerNote,amount;
    private String tot_package,tot_volume,tot_weight;
    private String note,officer_note;
    private String amount_spec;
    private String ordered_date,ordered_time;
    private String is_air,is_colombo,is_door;
    private String sl_address,sl_name,sl_contact,sl_pp_no;
    private String pp_no;
    private String paid;

    public String getLankanCharges() {
        return lankanCharges;
    }

    public void setLankanCharges(String lankanCharges) {
        this.lankanCharges = lankanCharges;
    }

    private String lankanCharges;

    public String getQid() {
        return qid;
    }

    public void setQid(String qid) {
        this.qid = qid;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }


    public String getOfficer_note() {
        return officer_note;
    }

    public void setOfficer_note(String officer_note) {
        this.officer_note = officer_note;
    }

    public String getAmount_spec() {
        return amount_spec;
    }

    public void setAmount_spec(String amount_spec) {
        this.amount_spec = amount_spec;
    }

    public String getOrdered_date() {
        return ordered_date;
    }

    public void setOrdered_date(String ordered_date) {
        this.ordered_date = ordered_date;
    }

    public String getOrdered_time() {
        return ordered_time;
    }

    public void setOrdered_time(String ordered_time) {
        this.ordered_time = ordered_time;
    }

    public String getIs_air() {
        return is_air;
    }

    public void setIs_air(String is_air) {
        this.is_air = is_air;
    }

    public String getIs_colombo() {
        return is_colombo;
    }

    public void setIs_colombo(String is_colombo) {
        this.is_colombo = is_colombo;
    }

    public String getIs_door() {
        return is_door;
    }

    public void setIs_door(String is_door) {
        this.is_door = is_door;
    }

    public String getSl_address() {
        return sl_address;
    }

    public void setSl_address(String sl_address) {
        this.sl_address = sl_address;
    }

    public String getSl_name() {
        return sl_name;
    }

    public void setSl_name(String sl_name) {
        this.sl_name = sl_name;
    }

    public String getSl_contact() {
        return sl_contact;
    }

    public void setSl_contact(String sl_contact) {
        this.sl_contact = sl_contact;
    }

    public String getSl_pp_no() {
        return sl_pp_no;
    }

    public void setSl_pp_no(String sl_pp_no) {
        this.sl_pp_no = sl_pp_no;
    }

    public String getPp_no() {
        return pp_no;
    }

    public void setPp_no(String pp_no) {
        this.pp_no = pp_no;
    }

    public String getPaid() {
        return paid+" QAR";
    }

    public void setPaid(String paid) {
        this.paid = paid;
    }

    public String getTot_package() {
        return "No.Packages "+tot_package;
    }

    public void setTot_package(String tot_package) {
        this.tot_package = tot_package;
    }

    public String getTot_volume() {
        return "Volume "+tot_volume;
    }

    public void setTot_volume(String tot_volume) {
        this.tot_volume = tot_volume;
    }

    public String getTot_weight() {
        return tot_weight+" Kg";
    }

    public void setTot_weight(String tot_weight) {
        this.tot_weight = tot_weight;
    }

    public Job() {}

    public String getAmount() {
        return amount+" ";
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLang() {
        return lang;
    }

    public void setLang(String lang) {
        this.lang = lang;
    }

    public String getContactNum() {
        return contactNum;
    }

    public void setContactNum(String contactNum) {
        this.contactNum = contactNum;
    }

    public String getPickupTime() {
        return pickupTime;
    }

    public void setPickupTime(String pickupTime) {
        this.pickupTime = pickupTime;
    }

    public String getPickupDate() {
        return pickupDate;
    }

    public void setPickupDate(String pickupDate) {
        this.pickupDate = pickupDate;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public String getPickerNote() {
        return pickerNote;
    }

    public void setPickerNote(String pickerNote) {
        this.pickerNote = pickerNote;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPickupedTime() {
        return pickupedTime;
    }

    public void setPickupedTime(String pickupedTime) {
        this.pickupedTime = pickupedTime;
    }

    public String getPickupedDate() {
        return pickupedDate;
    }

    public void setPickupedDate(String pickupedDate) {
        this.pickupedDate = pickupedDate;
    }

    public ArrayList<Packs> getPacks() {
        return packs;
    }

    public void setPacks(ArrayList<Packs> packs) {
        this.packs = packs;
    }
}
