package com.techfliks.laksiridriver.Models;


public class Packs {
    private String descr,size,qty,cmb,remarks,weight;

    public String getDescr() {
        return descr;
    }

    public void setDescr(String descr) {
        this.descr = descr;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getQty() {
        return qty;
    }

    public void setQty(String qty) {
        this.qty = qty;
    }

    public String getCmb() {
        return cmb;
    }

    public void setCmb(String cmb) {
        this.cmb = cmb;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public String getWeight() {
        return weight+" Kg";
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }
}
